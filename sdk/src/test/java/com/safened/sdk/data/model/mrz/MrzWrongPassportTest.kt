package com.safened.sdk.data.model.mrz

import com.safened.sdk.data.model.Mrz
import com.safened.sdk.data.model.Mrz.Companion.TYPE_TD3
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MrzWrongPassportTest {

    private lateinit var mrz: Mrz

    @BeforeEach
    fun setUp() {
        mrz = Mrz(
            "P<GBRMASON<<DELILAH<<<<<<<<<<<<<<<<<<<<<<<<<\n" +
                    "8703385446GBR4808259F1601019<<<<<<<<<<<<<<06"
        )
    }

    @Test
    fun isValid() {
        Assertions.assertFalse(mrz.isValid())
    }
}