package com.safened.sdk.data.model.mrz

import com.safened.sdk.data.model.Mrz
import com.safened.sdk.data.model.Mrz.Companion.TYPE_TD3
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MrzPassport2Test {

    private lateinit var mrz: Mrz

    @BeforeEach
    fun setUp() {
        mrz = Mrz(
            "P<GBRSTONE<<SARAH<<<<<<<<<<<<<<<<<<<<<<<<<<<\n" +
                    "0689349234GBR3708248F1601013<<<<<<<<<<<<<<06"
        )
    }

    @Test
    fun getType() {
        assert(mrz.type == TYPE_TD3)
    }

    @Test
    fun getDocumentNumber() {
        assert(mrz.documentNumber == "068934923")
    }

    @Test
    fun getFirstName() {
        assert(mrz.firstName == "Sarah")
    }

    @Test
    fun getLastName() {
        assert(mrz.lastName == "Stone")
    }

    @Test
    fun getNationality() {
        assert(mrz.nationality == "GBR")
    }

    @Test
    fun isValid() {
        assert(mrz.isValid())
    }
}