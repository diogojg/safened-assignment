package com.safened.sdk.data.model.mrz

import com.safened.sdk.data.model.Mrz
import com.safened.sdk.data.model.Mrz.Companion.TYPE_TD3
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MrzPassportTest {

    private lateinit var mrz: Mrz

    @BeforeEach
    fun setUp() {
        mrz = Mrz(
            "P<GBRMASON<<DELILAH<<<<<<<<<<<<<<<<<<<<<<<<<\n" +
                    "8703385446GBR4808259F1601013<<<<<<<<<<<<<<06"
        )
    }

    @Test
    fun getType() {
        assert(mrz.type == TYPE_TD3)
    }

    @Test
    fun getDocumentNumber() {
        assert(mrz.documentNumber == "870338544")
    }

    @Test
    fun getFirstName() {
        assert(mrz.firstName == "Delilah")
    }

    @Test
    fun getLastName() {
        assert(mrz.lastName == "Mason")
    }

    @Test
    fun getNationality() {
        assert(mrz.nationality == "GBR")
    }

    @Test
    fun getDateOfBirth() {
        assert(mrz.dateOfBirth == "480825")
    }

    @Test
    fun isValid() {
        assert(mrz.isValid())
    }
}