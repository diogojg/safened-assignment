package com.safened.sdk.data.model.mrz

import com.safened.sdk.data.model.Mrz
import com.safened.sdk.data.model.Mrz.Companion.TYPE_TD3
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MrzPassport3Test {

    private lateinit var mrz: Mrz

    @BeforeEach
    fun setUp() {
        mrz = Mrz(
            "P<GBRMENDOZA<<SYDNEY<WEBER<<<<<<<<<<<<<<<<<<\n" +
                    "4734816753GBR4303075F1601013<<<<<<<<<<<<<<02"
        )
    }

    @Test
    fun getType() {
        assert(mrz.type == TYPE_TD3)
    }

    @Test
    fun getDocumentNumber() {
        assert(mrz.documentNumber == "473481675")
    }

    @Test
    fun getFirstName() {
        assert(mrz.firstName == "Sydney Weber")
    }

    @Test
    fun getLastName() {
        assert(mrz.lastName == "Mendoza")
    }

    @Test
    fun getNationality() {
        assert(mrz.nationality == "GBR")
    }

    @Test
    fun getDateOfBirth() {
        assert(mrz.dateOfBirth == "430307")
    }

    @Test
    fun isValid() {
        assert(mrz.isValid())
    }
}