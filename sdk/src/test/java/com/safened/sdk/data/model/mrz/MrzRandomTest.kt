package com.safened.sdk.data.model.mrz

import com.safened.sdk.data.model.Mrz
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MrzRandomTest {

    private lateinit var mrz: Mrz

    @BeforeEach
    fun setUp() {
        mrz = Mrz("!@#^&*()!@#%^&*()!@#^&*()!@#%^\n" +
                        "@#^&*()!@#%^&*()!@#^&*()!@#%^!\n" +
                        "#^&*()!@#%^&*()!@#^&*()!@#%^!@")
    }

    @Test
    fun isValid() {
        Assertions.assertFalse(mrz.isValid())
    }
}