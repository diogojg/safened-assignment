package com.safened.sdk.data.model

/**
 * @author Diogo Garcia
 */
class MrpParser(private val text: String) {

    val documentCode: String
        get() = documentCodeSubstring(firstLine).stripFillingChars()

    val country: String
        get() = countrySubstring(firstLine).stripFillingChars()

    val firstName: String
        get() = firstNamesSubstring(namesSubstring)
            .replace("<", " ")
            .trim()
            .toLowerCase()
            .capitalizeWords()

    val lastName: String
        get() = lastNameSubstring(namesSubstring)
            .toLowerCase()
            .capitalizeWords()

    val documentNumber: String
        get() = documentNumberSubstring(secondLine).stripFillingChars()

    val nationality: String
        get() = nationalitySubstring(secondLine).stripFillingChars()

    val dateOfBirth: String
        get() = dateOfBirthSubstring(secondLine)

    val sex: String
        get() = sexSubstring(secondLine).stripFillingChars()

    val expirationDate: String
        get() = expirationDateSubstring(secondLine)

    val documentNumberChecksum: String
        get() = secondLine[9].toString()

    val dateOfBirthChecksum: String
        get() = secondLine[19].toString()

    val expirationDateChecksum: String
        get() = secondLine[27].toString()

    private val firstLine: String
        get() = text.lines()[0]

    private val secondLine: String
        get() = text.lines()[1]

    private val namesSubstring: String
        get() = namesSubstring(firstLine)

    private fun documentCodeSubstring(firstLine: String): String = firstLine.substring(0, 2)

    private fun countrySubstring(firstLine: String): String = firstLine.substring(2, 5)

    private fun namesSubstring(firstLine: String): String = firstLine.substring(5, 44)

    private fun lastNameSubstring(namesSubstring: String): String = namesSubstring.substringBefore("<<")

    private fun firstNamesSubstring(namesSubstring: String): String = namesSubstring.substringAfter("<<")

    private fun documentNumberSubstring(secondLine: String): String = secondLine.substring(0, 9)

    private fun nationalitySubstring(secondLine: String): String = secondLine.substring(10, 13)

    private fun dateOfBirthSubstring(secondLine: String): String = secondLine.substring(13, 19)

    private fun sexSubstring(secondLine: String): String = secondLine.substring(20, 21)

    private fun expirationDateSubstring(secondLine: String): String = secondLine.substring(21, 27)
}