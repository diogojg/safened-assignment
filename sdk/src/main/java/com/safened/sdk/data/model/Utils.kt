package com.safened.sdk.data.model

fun String.stripFillingChars(): String = replace("<", "")

fun String.capitalizeWords(): String {
    var capitalizedWords = ""
    split(" ").forEach { capitalizedWords += "${it.capitalize()} " }
    return capitalizedWords.trim()
}