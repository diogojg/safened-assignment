package com.safened.sdk.data.model

fun String.checksum(): Int {
    val checksumTarget = this.toUpperCase()
    var checksumValue = 0

    for (i in 0 until length) {
        val char = checksumTarget[i]
        checksumValue += (charValue(char) * charWeight(i))
    }
    return checksumValue % 10
}

private fun charValue(char: Char): Int {
    return if (char.isDigit())
        char.toString().toInt()
    else
        letterValue(char)
}

private fun letterValue(letterChar: Char): Int {
    var letterValue = 10
    for (letter in 'A'..'Z') {
        if (letter == letterChar) return letterValue
        letterValue++
    }
    return 0
}

private fun charWeight(index: Int): Int {
    val remainValue = (index + 1) % 3
    return when (remainValue) {
        1 -> 7
        2 -> 3
        0 -> 1
        else -> 0
    }
}