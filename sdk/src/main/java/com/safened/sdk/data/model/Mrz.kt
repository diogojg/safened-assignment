package com.safened.sdk.data.model

import android.support.annotation.IntDef

class Mrz(private val text: String) {

    @MrzType
    val type: Int
    val documentNumber: String
    val documentCode: String
    val firstName: String
    val lastName: String
    val country: String
    val nationality: String
    val sex: String
    val dateOfBirth: String
    val expirationDate: String
    val documentNumberChecksum: String
    val dateOfBirthChecksum: String
    val expirationDateChecksum: String

    @IntDef(TYPE_UNKNOWN, TYPE_TD1, TYPE_TD2, TYPE_TD3, TYPE_MRV_A, TYPE_MRV_B)
    @Retention(AnnotationRetention.SOURCE)
    annotation class MrzType

    companion object {
        const val TYPE_UNKNOWN = -1

        const val TYPE_TD1 = 1
        const val TYPE_TD2 = 2
        const val TYPE_TD3 = 3

        const val TYPE_MRV_A = 4
        const val TYPE_MRV_B = 5
    }

    init {
        // Define which type of document is and parse accordingly
        if (isValidPassportFormat(text)) {
            val parser = MrpParser(text)
            type = type()
            documentCode = parser.documentCode
            country = parser.country
            lastName = parser.lastName
            firstName = parser.firstName
            documentNumber = parser.documentNumber
            nationality = parser.nationality
            dateOfBirth = parser.dateOfBirth
            sex = parser.sex
            expirationDate = parser.expirationDate
            documentNumberChecksum = parser.documentNumberChecksum
            dateOfBirthChecksum = parser.dateOfBirthChecksum
            expirationDateChecksum = parser.expirationDateChecksum
        } else {
            type = TYPE_UNKNOWN
            documentCode = ""
            country = ""
            lastName = ""
            firstName = ""
            documentNumber = ""
            nationality = ""
            dateOfBirth = ""
            sex = ""
            expirationDate = ""
            expirationDateChecksum = ""
            documentNumberChecksum = ""
            dateOfBirthChecksum = ""
        }
    }

    @MrzType
    private fun type(): Int = when {
        text.isBlank() -> TYPE_UNKNOWN
        text[0] == 'P' -> TYPE_TD3
        else -> TYPE_UNKNOWN
    }

    fun isValid(): Boolean {
        return documentCode == "P"
                && type == TYPE_TD3
                && country.isAlphaString(false)
                && lastName.isAlphaString(false)
                && firstName.isAlphaString(true)
                && documentNumber.isAlphaNumString()
                && nationality.isAlphaString(false)
                && dateOfBirth.isValidDate()
                && (sex == "M" || sex == "F" || sex == "")
                && expirationDate.isValidDate()
                && (text.lines().size == 2)
                && (expirationDateChecksum.toInt() == expirationDate.checksum())
                && (documentNumberChecksum.toInt() == documentNumber.checksum())
                && (dateOfBirthChecksum.toInt() == dateOfBirth.checksum())
    }
}