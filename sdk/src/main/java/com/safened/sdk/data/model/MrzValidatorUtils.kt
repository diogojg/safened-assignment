package com.safened.sdk.data.model

import java.text.ParseException
import java.text.SimpleDateFormat

class MrzPassportValidator {
    companion object {
        const val ALPHA_WITH_SPACES_REGEX = "^[A-Za-z\\s]+[A-Za-z\\s]"
        const val ALPHA_NO_SPACES_REGEX = "[a-zA-Z][a-zA-Z]+"
        const val ALPHA_NUMERIC_REGEX = "([A-Za-z0-9]+)"
    }
}

fun isValidPassportFormat(mrzText: String): Boolean {
    val lines = mrzText.lines()
    return lines.size == 2
            && lines[0].length == 44
            && lines[1].length == 44
            && lines[0][0] == 'P'
}

fun String.isAlphaString(allowSpaces: Boolean): Boolean {
    return if (allowSpaces) matches(MrzPassportValidator.ALPHA_WITH_SPACES_REGEX.toRegex())
    else matches(MrzPassportValidator.ALPHA_NO_SPACES_REGEX.toRegex())
}

fun String.isAlphaNumString(): Boolean = matches(MrzPassportValidator.ALPHA_NUMERIC_REGEX.toRegex())

fun String.isValidDate(): Boolean {
    return try {
        val format = SimpleDateFormat("yyMMdd")
        format.parse(this)
        true
    } catch (e: ParseException) {
        false
    }
}